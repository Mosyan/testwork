import org.testng.annotations.Test;

@Test
public class DataChangerTest extends DataChanger {
    private String s="Шкаф:Доска,Гвоздь,Фанера;Тумбочка:Доска,Гвоздь,Направляющая;Стеллаж:Профиль,Болт;Пенал:Доска,Болт,Доводчик;Кровать:Доска,Профиль,Гвоздь;";

    public void process(){
        System.out.println(changer(s));
        System.out.println(changerWithGeneric(s));
    }
}
