import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.Map.Entry;

/**
 * Класс, содержащий методы для сортировки данных
 */
class DataChanger {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * метод позволяет считать строку с консоли.
     *
     * @return эту строку.
     */
    static String getStringData() {
        String s = null;
        try {
            s = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * метод закрывает поток ввода с консоли reader.
     */
    static void readerClose() {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * метод сортирует данные в соответствии с тестовым заданием.
     * данный метод дополнительно сортирует Изделия.
     *
     * @param input строка данных, которую нужно отсортировать.
     * @return эта же строка в отсортированном виде.
     */
    static String changer(String input) {
        StringBuilder output = new StringBuilder();
        try {
            if (!input.contains(";")) {
                throw new FormatException("формат не соответствует заявленному. Отсутствует ;");
            }
            List<String> list = Arrays.asList(input.trim().split(";"));
            Map<String, String> map = new TreeMap<>();
            Set<String> set = new TreeSet<>();
            for (String s : list) {
                if (!s.contains(":")) {
                    throw new FormatException("формат не соответствует заявленному. Отсутствует :");
                }
                if (s.split(":").length != 2) {
                    throw new FormatException("формат не соответствует заявленному. Двоеточие должно разделять изделие и материал");
                }
                map.put(s.split(":")[0], s.split(":")[1]);
                set.addAll(Arrays.asList(s.split(":")[1].split(",")));
            }
            for (String s : set) {
                output.append(s).append(":");
                for (Entry<String, String> entry : map.entrySet()) {
                    if (entry.getValue().contains(s)) {
                        output.append(entry.getKey()).append(",");
                    }
                }
                output.deleteCharAt(output.lastIndexOf(",")).append(";");
            }
        } catch (FormatException e) {
            e.printStackTrace();
        }
        return output.toString();
    }

    /**
     * метод сортирует данные в соответствии с тестовым заданием.
     *
     * @param input строка данных, которую нужно отсортировать.
     * @return эта же строка в отсортированном виде.
     */
    static String changerWithGeneric(String input) {
        StringBuilder output = new StringBuilder();
        try {
            if (!input.contains(";")) {
                throw new FormatException("формат не соответствует заявленному. Отсутствует ;");
            }
            List<String> list = Arrays.asList(input.trim().split(";"));
            List<Pair<String, String>> pairList = new ArrayList<>();
            Set<String> set = new TreeSet<>();
            for (String s : list) {
                if (!s.contains(":")) {
                    throw new FormatException("формат не соответствует заявленному. Отсутствует :");
                }
                if (s.split(":").length != 2) {
                    throw new FormatException("формат не соответствует заявленному. Двоеточие должно разделять изделие и материал");
                }
                pairList.add(new Pair<>(s.split(":")[0], s.split(":")[1]));
                set.addAll(Arrays.asList(s.split(":")[1].split(",")));
            }
            for (String s : set) {
                output.append(s).append(":");
                for (Pair<String, String> pair : pairList) {
                    if (pair.getR().contains(s)) {
                        output.append(pair.getL()).append(",");
                    }
                }
                output.deleteCharAt(output.lastIndexOf(",")).append(";");
            }
        } catch (FormatException e) {
            e.printStackTrace();
        }
        return output.toString();
    }


    public static class Pair<L, R> {
        private L l;
        private R r;

        public Pair(L l, R r) {
            this.l = l;
            this.r = r;
        }

        public L getL() {
            return l;
        }

        public void setL(L l) {
            this.l = l;
        }

        public R getR() {
            return r;
        }

        public void setR(R r) {
            this.r = r;
        }
    }

}
